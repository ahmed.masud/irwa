all: _book/book.pdf _book/index.html

_book/book.pdf: *.Rmd
	rm -f _main.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_book')"
	mv _book/_main.pdf $@

_book/tufte.pdf: *.Rmd
	rm -f _main.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::tufte_book2')"
	mv _book/_main.pdf $@

_book/index.html: *.Rmd
	rm -f _main.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"
	for f in _book/*.html; do sed -i -e '/^<head>/r ../ga-code.js' $$f; done

_book/tufte.html: *.Rmd
	rm -f _main.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'tufte::tufte_html')"
	mv _main.html $@

deploy:
	# Assumes ssh agent vars are present in the environment.
	rsync --exclude='*.swp' -avz _book/ booksite:/srv/erwabook.com/www-static/intro/
